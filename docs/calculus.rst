Quick calculation tools
=======================

.. currentmodule:: qsotools

Quasar coordinates
------------------
		   
.. autofunction:: coordinates

Spherical distance
------------------
		   
.. autofunction:: distance

Cosmological calculator
-----------------------
		   
.. autofunction:: coscal
		  
Lyman-alpha wavelength edges
----------------------------
		   
.. autofunction:: edges
		  
D/H to Omega baryon conversion
------------------------------
		   
.. autofunction:: omegab
