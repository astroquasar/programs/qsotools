:orphan:

Spectrum
=========

.. currentmodule:: qsotools.spec
   
.. autoclass:: Spectrum
   :no-inherited-members:
   :noindex:

     None

   .. rubric:: Methods Summary

   .. autosummary::

      ~__repr__
      ~multiply
      ~plot
      ~stats
      ~rebin
      ~rebin_simple
      ~write

   .. rubric:: Methods Documentation

   .. automethod:: __repr__
   .. automethod:: multiply
   .. automethod:: plot
   .. automethod:: stats
   .. automethod:: rebin
   .. automethod:: rebin_simple
   .. automethod:: write
