Plotting tools
==============

.. currentmodule:: qsotools

Full spectrum display
---------------------
		   
.. autofunction:: fullspec
