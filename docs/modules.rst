Modules
-------

Spectrum module from Barak
''''''''''''''''''''''''''

The class and functions in this module were built by Neil Crighton and are copies of the ones available in Neil's ``Barak`` package. The documentation below can also be found in the original package's `website <https://nhmc.github.io/Barak/index.html#spec>`_.

.. currentmodule:: qsotools.spec

.. autosummary::

   Spectrum

.. autosummary::
   :toctree: generated/

   find_wa_edges
   rebin
   combine



Voigt profile
'''''''''''''

.. currentmodule:: qsotools.voigt

.. autosummary::
   :toctree: generated/

   voigtslow
   voigt
   p_voigt
   voigtking
   voigt_model


Zoom effect
'''''''''''

`Zoom effect functions from Matplotlib <https://matplotlib.org/3.1.0/gallery/subplots_axes_and_figures/axes_zoom_effect.html>`_

.. currentmodule:: qsotools.zoom

.. autosummary::
   :toctree: generated/

   connect_bbox
   zoom_effect01
   zoom_effect02

