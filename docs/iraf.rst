Image Reduction and Analysis Facility (IRAF)
============================================

IRAF is becoming more and more difficult to install. In this page, we provide specific guidelines that will help you to successfully install the software on your machine.

macOS 10.14 and earlier
-----------------------

This guidelines below follow the ones available in `this webpage <https://research.iac.es/sieinvens/siepedia/pmwiki.php?n=HOWTOs.IrafMacOSX>`_.

macOS Catalina
--------------

If you have upgraded to macOS Catalina, you will very likely have an issue installing IRAF as some frameworks, like ``xgterm`` no longer work on the new OS. Following the workaround suggesting in `this page <https://medium.com/@krisastern/installing-iraf-pyraf-on-mac-with-linux-vm-767fb99ec757>`_, we have built an image of the Ubuntu 20.04.1 LTS operating system with IRAF pre-installed so just have to load it in `VirtualBox <http://virtualbox.org/>`_.
		
