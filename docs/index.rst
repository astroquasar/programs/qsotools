.. title:: Docs

.. image:: _images/qsotools.png
   :target: index.html
   :width: 50%

.. raw:: html

   <br>
      
.. image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://gitlab.com/astroquasar/programs/qsotools/-/raw/master/LICENSE
.. image:: https://badge.fury.io/py/qsotools.svg
   :target: https://pypi.python.org/pypi/qsotools/
.. image:: https://img.shields.io/pypi/dm/qsotools
.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.597138.svg
   :target: https://doi.org/10.5281/zenodo.597138

This documentation aims to provide detailed information regarding the use of the QSOTOOLS program. The program provides a large number of tools to help researchers to speed up their quasar spectra data analysis. This program was built by `Vincent Dumont <https://vincentdumont.gitlab.io/>`_ and is part of the `AstroQuasar <https://astroquasar.gitlab.io/>`_ group's toolset available in our `GitLab group <https://gitlab.com/astroquasar/programs/qsotools>`_.

Installation
------------

The package is built around 5 dependent libraries (``astropy``, ``matplotlib``, ``numpy``, ``pandas``, and ``scipy``) and can be easily installed via the ``pip`` Python package manager as follows::

  sudo pip install qsotools
		
Available operations
--------------------

The program can be executed by simply typing ``qso`` in the terminal, the following operations are available:

.. currentmodule:: qsotools

.. autosummary::

   coordinates
   distance
   coscal
   edges
   omegab
   rydberg
   z2dv
   w2dv
   zshift
   wshift
   alphadist
   fullspec
   
.. raw:: html

      <br>
      
Program Execution
-----------------

Below is the help message that will display if requested:

.. code-block:: text

   usage: qso [-h] {coordinates,distance,coscal,edges} ...
   
   Toolset for quasar absorption line analysis.
   
   positional arguments:
     {coordinates,distance,coscal,edges}
       coordinates         Convert coordinates
       distance            Spherical distance between 2 coordinates
       coscal              Cosmological calculator
       edges               Wavelength edges around Lyman-alpha
   
   optional arguments:
     -h, --help            show this help message and exit
   
   See '<command> --help' to read about a specific sub-command.

How to cite QSOTOOLS?
---------------------

Please acknowledge QSOTOOLS in your publications using the following citation:

.. code-block:: latex

  @software{qscan,
    author       = {Vincent Dumont},
    title        = {QScan: Quasar spectra scanning tool},
    month        = mar,
    year         = 2017,
    publisher    = {Zenodo},
    version      = {v1.0.0},
    doi          = {10.5281/zenodo.597138},
    url          = {https://doi.org/10.5281/zenodo.597138}
  }

License Agreement
-----------------

.. literalinclude:: ../LICENSE
   :language: html   

Reporting issues
----------------

.. raw:: html

   <a href="https://gitlab.com/astroquasar/programs/qsotools/-/issues" class="button3">
   <font color="#ffffff">Submit a ticket</font>
   </a>

If you find any bugs when running this program, please make sure to report them by clicking on the above link and submit a ticket on the software's official Gitlab repository.

.. toctree::
   :caption: Operations
   :hidden:

   calculus
   plots
   
.. toctree::
   :caption: Utilities
   :hidden:

   modules
   
.. toctree::
   :caption: Miscellaneous
   :hidden:

   iraf
