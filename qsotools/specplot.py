# System
import re

# External
import matplotlib.pyplot as plt

# Local
from .utils import get_data

def fullspec(spectrum,nrows=9,snr=False,show=False,pdf=False,**kwargs):
    """
    Plot full spectrum of quasar. The figure is split into ``nrows`` subplots.
    The top subplot shows the entire spectrum while the remaining subplots
    show consecutive segments of the spectrum.

    Parameters
    ----------
    spectrum : :class:`str`
      Path to quasar spectrum
    nrows : :class:`int`
      Total number of subplots
    snr : :class:`bool`
      Display signal-to-noise ratio
    show : :class:`bool`
      Inline display of figure instead of saving to file
    pdf : :class:`bool`
      Save as PDF, images saved as PNG by default
    
    Examples
    --------
    >>> import qso
    >>> qso.fullspec('J042214-384452.fits')

    .. image:: _images/fullspec.png
    """
    plt.style.use('seaborn')
    qsoname  = re.split(r'[/.]',spectrum)[-2]
    data = get_data(spectrum)
    fig = plt.figure(figsize=(10,15),dpi=200)
    plt.axis('off')
    plt.title(spectrum+'\n',fontsize=15)
    # Plot whole spectrum
    wmin,wmax = data['wa'][0],data['wa'][-1]
    ymin,ymax =  -0.5,sorted(data['fl'])[int(0.99*len(data['fl']))]+0.5
    ymin1,ymax1 = -1,sorted(data['fl'])[int(0.99*len(data['fl']))]+0.5
    ymin2,ymax2 = -1,max(data['fl']/data['er'])
    ax = fig.add_subplot(nrows,1,1)
    ax.plot(data['wa'],data['fl'],'black',lw=1)
    ax.plot(data['wa'],data['er'],'cyan',lw=1)
    ax.set_xlim([wmin,wmax])
    ax.set_ylim([ymin,ymax])
    ax.text(data['wa'][0]+0.85*(data['wa'][-1]-data['wa'][0]),-0.7,'Whole spectrum',weight='bold',size=10,color='red')
    ax.set_ylabel('Flux')
    ax.axhline(y=0,color='red',ls='dashed',lw=1)
    ax.axhline(y=1,color='red',ls='dashed',lw=1)    
    if snr:
        ax = ax.twinx()
        ax.plot(data['wa'],data['fl']/data['er'],color='red',lw=0.1)
        ax.set_xlim([wmin,wmax])
        ax.set_ylim([ymin2,ymax2])
        ax.set_ylabel('Signal-to-noise')
    # Plot separate range of the spectrum
    waveint = (data['wa'][-1]-data['wa'][0])/(nrows-1)
    istart  = 0
    wmin    = data['wa'][istart]
    wmax    = data['wa'][istart] + waveint
    iend    = abs(data['wa'] - wmax).argmin()
    for i in range(2,nrows+1):
        ax = fig.add_subplot(nrows,1,i)
        y1 = data['fl'][istart:iend]
        ax.plot(data['wa'][istart:iend],data['fl'][istart:iend],'black',lw=1)
        ax.plot(data['wa'][istart:iend],data['er'][istart:iend],'cyan',lw=1)
        ax.set_xlim([wmin,wmax])
        ax.set_ylim([ymin,ymax])
        ax.set_ylabel('Flux')
        ax.axhline(y=0,color='red',ls='dashed',lw=1)
        ax.axhline(y=1,color='red',ls='dashed',lw=1)
        if snr:
            ax = ax.twinx()
            ax.plot(data['wa'][istart:iend],data['fl'][istart:iend]/data['er'][istart:iend],color='lime',lw=0.1)
            ax.set_xlim([wmin,wmax])
            ax.set_ylim([ymin2,ymax2])
            ax.set_ylabel('Signal-to-noise')
        istart = iend
        wmin   = wmax
        wmax   = wmin + waveint
        iend   = abs(data['wa'] - wmax).argmin()
    plt.tight_layout()
    if show:
        plt.show()
    else:
        plt.savefig(qsoname+('.pdf' if pdf else '.png'))
        
