#######################################################################
# Copyright (c) 2019, Quasar Astronomy Group.
#
# Produced at Lawrence Berkeley National Laboratory.
# Written by V. Dumont (vincentdumont11@gmail.com).
# All rights reserved.
#
# This file is part of the QSOTOOLS software.
# For details, see gitlab.io/astroquasar/qsotools
# For details about use and distribution, please read QSOTOOLS/LICENSE.
#######################################################################

# System
import re
import sys
import math

# External
import numpy
from scipy import integrate

# Local
from .constants import *
from .utils import spherical_distance

def coordinates(coord,xdipp=17.3,ydipp=-61,**kwargs):
    '''
    Convert coordinates and get distance to alpha dipole. Right ascension and
    declination are converted into different formats so that either of the
    outputs can be used for analysis.

    Parameters
    ----------
    coord : :class:`str`
      Coordinates of object. 3 formats are accepted, either copy/paste from
      SIMBAD with either colon or space separating the degree, minutes and
      seconds, or coordinates in decimal degrees are also accepted.
    xdipp : :class:`float`
      Right ascension of alpha dipole (in degrees)
    ydipp : :class:`float`
      Declination of alpha dipole (in degrees)

    Returns
    -------
    ra : :class:`float`
      Right ascension in degrees
    dec : :class:`float`
      Declination in degrees

    Examples
    --------
    >>> qso coordinates 00 15 45.2 +22 01 34
    RA  :  00:15:45.200 |   0.262556h |   3.938333d
    DEC : +22:01:33.000 |  22.026111d |  22.026111d
    Distance to dipole (259.5,  -61)      : 138.5192 degrees
    Distance to anti-dipole (439.5,   61) :  41.4808 degrees

    >>> qso coordinates 00:15:45.2 +22:01:34
    RA  :  00:15:45.200 |   0.2626h |   3.9383d
    DEC : +22:01:33.000 |  22.0261d |  22.0261d
    Distance to dipole (259.5,  -61)      : 138.5192 degrees
    Distance to anti-dipole (439.5,   61) :  41.4808 degrees

    The arguments ``--xdipp`` and ``--ydipp`` can be used to customize the
    location of the dipole.
    '''
    if len(coord)%6==0:
        ra, dec =  coord[:3], coord[3:6]
        # Decompose right ascension values
        ra   = float(ra[0])+float(ra[1])/60+float(ra[2])/3600
        hrs  = float(str(abs(ra)).split('.')[0])
        mins = float(str(abs(60.*(ra-hrs))).split('.')[0])
        sec1 = float(str(abs(3600.*(ra-hrs-mins/60.))).split('.')[0])
        sec2 = ('%.3f'%float(abs(3600.*(ra-hrs-mins/60.)-sec1))).split('.')[1]
        ra2  = '%02i:%02i:%02i.'%(hrs,mins,sec1)+sec2
        # Decompose declinaison values
        sign = -1 if float(dec[0])<0 else 1
        dec  = float(dec[0])+sign*float(dec[1])/60+sign*float(dec[2])/3600
        sign = '-' if dec<0 else '+'
        degs = float(str(abs(dec)).split('.')[0])
        mins = float(str(abs(60.*(abs(dec)-degs))).split('.')[0])
        sec1 = float(str(abs(3600.*(abs(dec)-degs-mins/60.))).split('.')[0])
        sec2 = ('%.3f'%float(abs(3600.*(dec-degs-mins/60.)-sec1))).split('.')[1]
        dec2 = sign+'%02i:%02i:%02i.'%(degs,mins,sec1)+sec2
        print('\n\tRA  : {0:>13} | {1:>10}h | {2:>10}d'.format(ra2,'%.6f'%ra,'%.6f'%(ra*360./24.)))
        print('\tDEC : {0:>13} | {1:>10}d | {2:>10}d'.format(dec2,'%.6f'%dec,'%.6f'%dec))
    elif ':' in coord[0]:
        ra, dec = coord[0], coord[1]
        # Decompose right ascension values
        ra   = ra.split(':')
        ra   = float(ra[0])+float(ra[1])/60+float(ra[2])/3600
        hrs  = float(str(abs(ra)).split('.')[0])
        mins = float(str(abs(60.*(ra-hrs))).split('.')[0])
        sec1 = float(str(abs(3600.*(ra-hrs-mins/60.))).split('.')[0])
        sec2 = ('%.3f'%float(abs(3600.*(ra-hrs-mins/60.)-sec1))).split('.')[1]
        ra2  = '%02i:%02i:%02i.'%(hrs,mins,sec1)+sec2
        # Decompose declinaison values
        dec  = dec.split(':')
        sign = -1 if float(dec[0])<0 else 1
        dec  = float(dec[0])+sign*float(dec[1])/60+sign*float(dec[2])/3600
        sign = '-' if dec<0 else '+'
        degs = float(str(abs(dec)).split('.')[0])
        mins = float(str(abs(60.*(dec-degs))).split('.')[0])
        sec1 = float(str(abs(3600.*(dec-degs-mins/60.))).split('.')[0])
        sec2 = ('%.3f'%float(abs(3600.*(dec-degs-mins/60.)-sec1))).split('.')[1]
        dec2 = sign+'%02i:%02i:%02i.'%(degs,mins,sec1)+sec2
        print('\n\tRA  : {0:>13} | {1:>8}h | {2:>8}d'.format(ra2,'%.4f'%ra,'%.4f'%(ra*360./24.)))
        print('\tDEC : {0:>13} | {1:>8}d | {2:>8}d'.format(dec2,'%.4f'%dec,'%.4f'%dec))
    else:
        ra, dec = coord[0], coord[1]
        # Decompose right ascension values
        ra   = float(re.compile(r'[a-zA-Z]+').sub('',ra))
        ra   = ra if 'h' in coord[0] else ra*24./360
        hrs  = float(str(abs(ra)).split('.')[0])
        mins = float(str(abs(60.*(ra-hrs))).split('.')[0])
        sec1 = float(str(abs(3600.*(ra-hrs-mins/60.))).split('.')[0])
        sec2 = ('%.3f'%float(abs(3600.*(ra-hrs-mins/60.)-sec1))).split('.')[1]
        ra2  = '%02i:%02i:%02i.'%(hrs,mins,sec1)+sec2
        # Decompose declinaison values
        dec  = float(re.compile(r'[a-zA-Z]+').sub('',dec))
        sign = '-' if dec<0 else '+'
        degs = float(str(abs(dec)).split('.')[0])
        mins = float(str(abs(60.*(abs(dec)-degs))).split('.')[0])
        sec1 = float(str(abs(3600.*(abs(dec)-degs-mins/60.))).split('.')[0])
        sec2 = ('%.3f'%float(abs(3600.*(abs(dec)-degs-mins/60.)-sec1))).split('.')[1]
        dec2 = sign+'%02i:%02i:%02i.'%(degs,mins,sec1)+sec2
        print('\n\tRA  : {0:>13} | {1:>8}h | {2:>8}d'.format(ra2,'%.4f'%ra,'%.4f'%(ra*360./24.)))
        print('\tDEC : {0:>13} | {1:>8}d | {2:>8}d'.format(dec2,'%.4f'%dec,'%.4f'%dec))
    ra    = ra*360./24.
    xdipp = xdipp*360/24
    xdipm = xdipp+180
    ydipm = -ydipp
    print('\n\tDistance to dipole ({:>5},{:>5})      : {:>8} degrees'.format(xdipp,ydipp,'%.4f'%spherical_distance(ra*360./24.,dec,xdipp,ydipp)))
    print('\tDistance to anti-dipole ({:>5},{:>5}) : {:>8} degrees\n'.format(xdipm,ydipm,'%.4f'%spherical_distance(ra*360./24.,dec,xdipm,ydipm)))
    return ra*360./24., dec
            
def distance(coord,**kwargs):
    '''
    Spherical distance between 2 coordinates

    Parameters
    ----------
    coord : :class:`str`
      Coordinates of object. 3 formats are accepted, either copy/paste from
      SIMBAD with either colon or space separating the degree, minutes and
      seconds, or coordinates in decimal degrees are also accepted.

    Examples
    --------
    >>> qso distance 00:15:45.2 +22:01:34 00:18:45.2 +23:01:34
    Object 1:
        RA  :  00:15:45.200 |   0.2626h |   3.9383d
    	DEC : +22:01:33.000 |  22.0261d |  22.0261d
    	Distance to dipole (259.5,  -61)      : 138.5192 degrees
    	Distance to anti-dipole (439.5,   61) :  41.4808 degrees
    Object 2:
    	RA  :  00:18:45.200 |   0.3126h |   4.6883d
    	DEC : +23:01:33.000 |  23.0261d |  23.0261d
    	Distance to dipole (259.5,  -61)      : 141.4977 degrees
    	Distance to anti-dipole (439.5,   61) :  38.5023 degrees
    Spherical distance between the 2 given coordinates: 10.4370 degrees.
    '''
    if len(coord)%6==0:    
        print('\nObject 1:')
        lon1, lat1 = coordinates(coord[:6])
        print('Object 2:')
        lon2, lat2 = coordinates(coord[6:12])
    else:
        print('\nObject 1:')
        lon1, lat1 = coordinates(coord[:2])
        print('Object 2:')
        lon2, lat2 = coordinates(coord[2:4])
    print('Spherical distance between the 2 given coordinates: %.4f degrees.\n'%spherical_distance(lon1,lat1,lon2,lat2))
        
def coscal(z,**kwargs):
    '''
    Cosmological Calculator.

    Parameters
    ----------
    z : :class:`float`
      Absorption redshift

    Examples
    --------
    >>> qso coscal 2.3
    Hubble Distance................  4222.429 Mpc
    Total LOS Comoving Distance....  5664.041 Mpc
    Transverse Comoving Distance...  5664.041 Mpc
    Angular Diameter Distance .....  1716.376 Mpc
    Luminosity Distance............ 18691.335 Mpc
    Distance Modulus...............    46.358
    Lookback Time..................    10.759 Gyrs
    Age of the Universe at z.......     3.039 Gyrs
    '''
    def e_z(z): return 1.0/math.sqrt(omega_m*((1+z)**3)+ omega_k*((1+z)**2) + omega_lambda)
    def lb_e_z(z): return 1.0/((1+z)*math.sqrt(omega_m*((1+z)**3)+ omega_k*((1+z)**2) + omega_lambda))
    H0 = 71     # 100*h Km/s/Mpc
    omega_m = 0.27
    omega_k = 0.
    omega_lambda = 0.73    
    H0_std = (H0/(3.08568025 * 10**19))  # sec-1
    d_h = c/H0    # Mpc
    e_z_int, e_z_int_err = integrate.quad(e_z,0.,z)
    d_c = d_h * e_z_int
    e_z_int2, e_z_int_err2 = integrate.quad(lb_e_z,0.,z)
    lbt = e_z_int2/H0_std/(3600*24*365.25*10**9)
    age = 13.798 - lbt
    if (omega_k==0.0):
        d_t = d_c
    elif (omega_k>0.0):
        d_t = d_h/math.sqrt(omega_k) * math.sinh(math.sqrt(omega_k)*d_c/d_h)
    else:
        d_t = d_h/math.sqrt(abs(omega_k)) * math.sinh(math.sqrt(abs(omega_k))*d_c/d_h)
    if (omega_lambda==0.0):
        d_t = d_h * 2 *(2 - (omega_m *(1-z)) - ((2-omega_m) * (math.sqrt(1+(omega_m*z))))) / (omega_m**2 * (1+z))
    d_a = d_t / (1+z)    
    d_l = (1+z) * d_t
    dm = 5.0 * numpy.log10(d_l*10**6/10)
    print('')
    print('Hubble Distance................ {:>9.3f} Mpc'.format(d_h))
    print('Total LOS Comoving Distance.... {:>9.3f} Mpc'.format(d_c))
    print('Transverse Comoving Distance... {:>9.3f} Mpc'.format(d_t))
    print('Angular Diameter Distance ..... {:>9.3f} Mpc'.format(d_a))
    print('Luminosity Distance............ {:>9.3f} Mpc'.format(d_l))
    print('Distance Modulus............... {:>9.3f}'.format(dm))
    print('Lookback Time.................. {:>9.3f} Gyrs'.format(lbt))
    print('Age of the Universe at z....... {:>9.3f} Gyrs'.format(age))
    print('')
        
def edges(z,v,**kwargs):
    '''
    Wavelength edges around given Lyman-alpha central redshift
    
    Parameters
    ----------
    z : :class:`float`
      Central redshift of Lyman-alpha
    v : :class:`float`
      Velocity dispersion

    Examples
    --------
    >>> qso edges 2.3 70
    A z = 2.3 HI Lyman-alpha system corresponds to lambda: 4011.71
    The 70.0 km/s region around that line is: 4011.24 - 4012.18
    '''
    wl = 1215.67*(z+1)
    dl = wl*(v/2)/c
    print('')
    print('A z =',z,'HI Lyman-alpha system corresponds to lambda:',round(wl,2))
    print('The',v,'km/s region around that line is:',round(wl-dl,2),'-',round(wl+dl,2))
    print('')
        
def omegab(dtoh,err,**kwargs):
    '''
    Get baryon density from D/H values

    Parameters
    ----------
    dtoh : :class:`float`
      10^-5 D/H
    err : :class:`float`
      Uncertainty on D/H

    Examples
    --------
    >>> qso omegab 2.535 0.05
    10^-5 D/H = 2.535 +/- 0.050
    Omega_b h^2 = 0.0223 +/- 0.0312

    References
    ----------
    Used calculations from `Pettini & Cooke (2012) <https://academic.oup.com/mnras/article/425/4/2477/1075167>`_.
    '''
    omega_b = 6. * (2.6 / dtoh)**(5./8.) / 273.9
    delta = (5./8.) * err
    print('  10^-5 D/H = %.3f +/- %.3f' % (dtoh,err))
    print('Omega_b h^2 = %.4f +/- %.4f' % (omega_b,delta))
        
def rydberg(args):
    '''
    Compute wavelength and energy of Rydberg atom
    '''
    me = 9.10938291 * 10**(-31)     # kg
    mp = 1.672621777 * 10**(-27)    # kg
    mn = 1.674927351 * 10**(-27)    # kg
    e  = 1.602176565 * 10**(-19)    # Coulomb
    pe = 8.854187817620 * 10**(-12) # F/m
    h  = 6.62606957 * 10**(-34)     # J.s
    c  = 299792458                  # m/s
    lambd = (me+(args.Np*mp+args.Nn*mn))/(me*(args.Np*mp+args.Nn*mn)) * (8*pe**2*h**3*c)/e**4 * 4/3
    E = (me*(args.Np*mp+args.Nn*mn))/(me+(args.Np*mp+args.Nn*mn)) * e**4/(8*pe**2*h**2) / e
    print('')
    print(args.Np,'proton and',args.Nn,'neutrons (use options for custom values):')
    print('Lambda =',lambd*10**10,'Angstrom')
    print('Energy =',E,'eV')
    print('')
            
def zdv(z1,z2,**kwargs):
    dv = 2 * (z2-z1) / (z2+z1+2) * c
    print('\nThe velocity dispersion betweeen redshifts %s and %s is %s km/s\n'%(z1,z2,dv))
    return dv
        
def wdv(w1,w2,**kwargs):
    '''
    Examples
    --------
    >>> quasar veldisp -w 1215.6701 1215.3394
    The velocity dispersion betweeen wavelengths 1215.6701 and 1215.3394 is -81.56394770205024 km/s
    '''
    dv = 2 * (w2-w1) / (w2+w1) * c
    print('\nThe velocity dispersion betweeen wavelengths %s and %s is %s km/s\n'%(w1,w2,dv))
    return dv
        
def zshift(z,dv,**kwargs):
    z_shifted = float(z) * (2*c+dv) / (2*c-dv) + 2*dv / (2*c-dv)
    print('\nThe shifted redshift is %s\n'%z_shifted)
    return z_shifted
    
def wshift(wa,dv,**kwargs):
    wa_shifted = float(wa) * (2*c+dv) / (2*c-dv)
    print('\nThe shifted wavelength is %s\n'%wa_shifted)
    return wa_shifted

def alphadist(args):
    '''
    Calculate da/a for distorted system
    '''
    # Reproducing value from John's table
    zabs = 0
    daoa = 1e-5        # da/a value to test
    wrest,q = 2796.3550990, 211.
    # Calculate the shifted wavelength due to da/a
    wobs   = wrest*(1+zabs)                # observed wavelength
    omega  = 10**8 / wrest                 # rest-frame wavenumber
    omega  = omega-q*(daoa**2-2*daoa)      # rest-frame wavenumber with da/a added 
    wshift = 10**8 / omega                 # rest-frame wavelength with da/a added
    wshift = wshift*(1+zabs)               # observed wavelength with da/a added
    print('---------------------------')
    print('    MgII 2796')
    print('---------------------------')
    print('  domega = %.2f 1/A'%((10**8/wshift-10**8/wobs)/10**-2))
    print(' dlambda = %.2f A'%((wshift-wobs)/10**-3))
    print('      dv = %.3f km/s'%((wshift-wobs)/wobs*args.c))
    # Do calculations
    zabs      = 1
    slope     = 0.2
    wrest1,q1 = 2026.1376450, 1584.
    wobs1     = wrest1*(1+zabs)
    dv1       = slope*(wobs1-5720.)
    wshifted1 = wobs1*(1+dv1/args.c)
    daoa1     = -1/2*dv1/1000/args.c*10**8/wobs1/q1
    print('---------------------------')
    print('    ZnII 2026')
    print('---------------------------')
    print('       q = %i'%q1)
    print('   wrest = %.3f A'%wrest1)
    print('    wobs = %.3f A'%wobs1)
    print('   shift = %i m/s'%dv1)
    print('   wdist = %.3f A'%wshifted1)
    print('   alpha = %.2E'%daoa1)
    wrest2,q2 = 2066.1638990, -1360.
    wobs2     = wrest2*(1+zabs)
    dv2       = slope*(wobs2-5720.)
    wshifted2 = wobs2*(1+dv2/args.c)
    daoa2     = -1/2*dv2/1000/args.c*10**8/wobs2/q2
    print('---------------------------')
    print('    CrII 2066')
    print('---------------------------')
    print('       q = %i'%q2)
    print('   wrest = %.3f A'%wrest2)
    print('    wobs = %.3f A'%wobs2)
    print('   shift = %i m/s'%dv2)
    print('   wdist = %.3f A'%wshifted2)
    print('   alpha = %.2E A'%daoa2)
    dq    = abs(q2-q1)
    ddv   = abs(dv2-dv1)
    mean  = (wrest1+wrest2)/2.
    ddaoa = ((1-10**8/mean*ddv/1000/dq/args.c)**0.5-1)/1E-6    
    print('---------------------------')
    print('   ZnII 2026 - CrII 2066')
    print('---------------------------')
    print('   wmean = %.3f A'%mean)        # mean wavelength
    print('      dq = %i'%dq)              # q contrast
    print('      dv = %i m/s'%ddv)         # corresponding velocity shift
    print('   sigma = %.3f ppm'%ddaoa)     # estimated error
    print('---------------------------')
