import os,numpy,sys
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *

class Q1937Highz:
    '''
    Velocity plot for `Riemer-Sorensen et al (2017) <https://doi.org/10.1093/mnras/stx681>`_.
    '''
    rc('font', size=2, family='sans-serif')
    rc('axes', labelsize=2, linewidth=0.5)
    rc('legend', fontsize=2, handlelength=10)
    rc('xtick', labelsize=8)
    rc('ytick', labelsize=8)
    rc('lines', lw=0.5, mew=0.2)
    rc('grid', linewidth=0.5)
    
    os.environ['VPFSETUP']='./vp_setup.dat'
    os.environ['ATOMDIR']='./atom.dat'
    
    k = 1.3806488*10**-23     # m^2.kg.s^-2.K^-1
    c = 299792.458            # km/s

    highz_header = numpy.array([['CII'  ,1334.53230],['CII'  ,1334.53230],['CII'  ,1334.53230],
                                ['CII'  ,1334.53230],['CII'  ,1334.53230],['FeIII',1122.52400],
                                ['FeIII',1122.52400],['FeIII',1122.52400],['FeIII',1122.52400],
                                ['FeIII',1122.52400],['SiIV', 1402.77291],['SiIV', 1402.77291],
                                ['SiIV', 1402.77291],['SiIV', 1393.76018],['SiIV', 1393.76018],
                                ['SiIV', 1393.76018],['SiIV', 1393.76018],['SiIV', 1393.76018],
                                ['SiII', 1304.37020],['SiII', 1304.37020],['SiII', 1304.37020],
                                ['SiII', 1304.37020],['SiII', 1304.37020],['SiII', 1193.28970],
                                ['SiII', 1193.28970],['SiII', 1193.28970],['SiII', 1193.28970],
                                ['SiII', 1193.28970],['HI',   1215.67010],['HI',   1215.67010],
                                ['HI',   1215.67010],['HI',   1215.67010],['HI',   1215.67010],
                                ['HI',   1025.72230],['HI',   1025.72230],['HI',   1025.72230],
                                ['HI',   1025.72230],['HI',    972.53680],['HI',    972.53680],
                                ['HI',    972.53680],['HI',    972.53680],['HI',    972.53680],
                                ['HI',    949.74310],['HI',    949.74310],['HI',    949.74310],
                                ['HI',    949.74310],['HI',    949.74310],['HI',    937.80350],
                                ['HI',    937.80350],['HI',    937.80350],['HI',    937.80350],
                                ['HI',    937.80350],['HI',    930.74830],['HI',    930.74830],
                                ['HI',    930.74830],['HI',    930.74830],['HI',    930.74830],
                                ['HI',    926.22570],['HI',    926.22570],['HI',    926.22570],
                                ['HI',    926.22570],['HI',    926.22570],['HI',    923.15040],
                                ['HI',    923.15040],['HI',    923.15040],['HI',    923.15040],
                                ['HI',    923.15040],['HI',    920.96310],['HI',    920.96310],
                                ['HI',    920.96310],['HI',    920.96310],['HI',    920.96310]],dtype=object)
    
    highz_atom = [['CII'  ,1334.53230],['FeIII',1122.52400],['SiIV' ,1402.77291],['SiIV' ,1393.76018],
                  ['SiII' ,1304.37020],['SiII' ,1193.28970],['HI'   ,1215.67010],['HI'   ,1025.72230],
                  ['HI'   , 972.53680],['HI'   , 949.74310],['HI'   , 937.80350],['HI'   , 930.74830],
                  ['HI'   , 926.22570],['HI'   , 923.15040],['HI'   , 920.96310],['DI'   ,1215.33940],
                  ['DI'   ,1025.44330],['DI'   , 972.27220],['DI'   , 949.48470],['DI'   , 937.54840],
                  ['DI'   , 930.49510],['DI'   , 925.97370],['DI'   , 922.89900],['DI'   , 920.71200]]
    
    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "description:"
        print ""
        print "  Do velocity plots of spectrum and model for the zabs=3.5724 absorption"
        print "  system toward Q1937 published."
        print ""
        print "usage:"
        print ""
        print "    quasar q1937_highz fort.26 chunks26/"
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()
    
    def __init__(self):
    
        self.logf   = open("PyD.log", "w")
        self.header = self.highz_header
        self.atom   = self.highz_atom
        fig = plt.figure(figsize=(10,8))
        self.fig = fig
        self.axlist = []
        plt.subplots_adjust(left=0.04, right=0.96, bottom=0.07, top=0.96, wspace=0, hspace=0)
        for k in range(2,len(sys.argv),2):
            self.fortfile = sys.argv[k]
            self.forttype = self.fortfile.split('.')[-1]
            if self.forttype=='13':
                readfort13()
            if self.forttype=='26':
                readfort26()
            createchunks(k)
            imin      = self.red.index(min(self.red))
            imax      = self.red.index(max(self.red))
            self.zmid = (self.red[imin]+self.red[imax])/2.
            tmax      = int(imax/2.)
            lmid      = self.table1[tmax][6]*(self.zmid+1)
            self.dv   = (self.table1[tmax][3]-lmid)/lmid*c
            i,p=0,1
            while (i<len(self.table1)):
                print k, self.table1[i][2],self.table1[i][3]
                print 'trans_'+self.table1[i][5]+'_'+str(round(float(self.table1[i][6]),2))+'.dat'
                self.logf.write('%i  %f  %f \n' % (k, self.table1[i][2], self.table1[i][3]))
                self.logf.write('%s \n' %('trans_'+self.table1[i][5]+'_'+str(round(float(self.table1[i][6]),2))+'.dat'))
                if k==2:
                    highz_plotsetup(k,i,p)
                i = createcoaddspec(k,i)
                highz_plot(k,i,p)
                p = p + 1
        savefig('coadd.pdf')
        os.system('open coadd.pdf')
        self.logf.close()
