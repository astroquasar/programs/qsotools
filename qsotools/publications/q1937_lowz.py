import os,numpy,sys,re
import matplotlib.pyplot as plt
from matplotlib import rc
from pylab import *
from barak import spec
from astropy.io import fits

class Q1937Lowz:
    '''
    Velocity plot for `Riemer-Sorensen et al (2015) <https://doi.org/10.1093/mnras/stu2599>`_.
    '''
    k = 1.3806488*10**-23     # m^2.kg.s^-2.K^-1
    c = 299792.458            # km/s
    datapath = os.path.abspath(__file__).rsplit('/', 1)[0] + '/data/'
    header = np.array([['SiIV',1393.76018],['SiIV',1393.76018],['SiIV',1393.76018],
                       ['SiIV',1393.76018],['SiIV',1402.77291],['SiIV',1402.77291],
                       ['SiIV',1402.77291],['SiIV',1402.77291],['CIV' ,1548.20490],
                       ['CIV' ,1548.20490],['CIV' ,1550.77845],['CIV' ,1550.77845],
                       ['CII' ,1334.53230],['CII' ,1334.53230],['CII' ,1334.53230],
                       ['CII' ,1334.53230],['CII' ,1036.33670],['CII' ,1036.33670],
                       ['CII' ,1036.33670],['SiII',1526.70698],['SiII',1526.70698],
                       ['SiII',1526.70698],['SiII',1260.42210],['SiII',1260.42210],
                       ['SiII',1260.42210],['SiII',1260.42210],['AlII',1670.78860],
                       ['HI'  ,1215.67010],['HI'  ,1215.67010],['HI'  ,1215.67010],
                       ['HI'  ,1215.67010],['HI'  ,1025.72230],['HI'  ,1025.72230],
                       ['HI'  ,1025.72230],['HI'  ,1025.72230],['OI'  , 988.77340]])
    atom = [['HI'  ,1215.67010],['HI'  ,1025.72230],['DI'  ,1215.33940],['DI'  ,1025.44330],
            ['CII' ,1334.53230],['CII' ,1036.33670],['CIV' ,1548.20490],['CIV' ,1550.77845],
            ['SiII',1260.42210],['SiII',1526.70698],['SiIV',1393.76018],['SiIV',1402.77291],
            ['AlII',1670.78860],['OI'  , 988.77340]]

    def showhelp(self):
        
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        print "description:"
        print ""
        print "  Do velocity plots of spectrum and model for the zabs=3.2559 absorption"
        print "  system toward Q1937 published."
        print ""
        print "-------------------------------------------------------------------------"
        print ""
        quit()

    def __init__(self):
        
        if '--help' in sys.argv or '-h' in sys.argv:
            self.showhelp()
        # Set up environment and files
        os.environ['VPFSETUP']='./vp_setup.dat'
        os.environ['ATOMDIR']='./atom.dat'
        os.system('cp -r '+self.datapath+' .')
        # Initialise figure
        rc('font', size=2, family='sans-serif')
        rc('axes', labelsize=2, linewidth=0.5)
        rc('legend', fontsize=2, handlelength=10)
        rc('xtick', labelsize=8)
        rc('ytick', labelsize=8)
        rc('lines', lw=0.5, mew=0.2)
        rc('grid', linewidth=0.5)
        fig = plt.figure(figsize=(10,8))
        # Loop through files to create layers in figures
        self.logf = open("PyD.log", "w")
        self.fig = fig
        plt.subplots_adjust(left=0.04, right=0.96, bottom=0.07, top=0.96, wspace=0, hspace=0)
        files = ['fort_reduced.26','fort.13','fort_reduced.26']
        for self.layer in range(len(files)):
            self.fortfile = files[self.layer]
            self.forttype = self.fortfile.split('.')[-1]
            if self.forttype=='13': self.readfort13()
            if self.forttype=='26': self.readfort26()
            self.createchunks()
            imin      = self.red.index(min(self.red))
            imax      = self.red.index(max(self.red))
            self.zmid = 3.2559387522
            tmax      = int(imax/2.)
            lmid      = self.table1[tmax][6]*(self.zmid+1)
            self.dv   = (self.table1[tmax][3]-lmid)/lmid*self.c
            i,p=0,1
            while (i<len(self.table1)):
                print self.table1[i][2],self.table1[i][3]
                print 'trans_'+self.table1[i][5]+'_'+str(round(float(self.table1[i][6]),2))+'.dat'
                self.logf.write('%s  %f  %f \n' % (self.fortfile, self.table1[i][2], self.table1[i][3]))
                self.logf.write('%s \n' %('trans_'+self.table1[i][5]+'_'+str(round(float(self.table1[i][6]),2))+'.dat'))
                self.plotsetup(i,p)
                i = self.createcoaddspec(i)
                self.plot(i)
                p = p + 1
        savefig('coadd.pdf')
        self.logf.close()

    def readfort13(self):
    
        fortline,fort = [],open(self.fortfile,'r')
        for line in fort:
            if len(line)==1: break
            elif line[0]!='!': fortline.append(line.replace('\n',''))
        self.table1,self.table2,self.red=[],[],[]
        i=1
        while fortline[i].split()[0]!='*':
            l        = fortline[i].split()
            fname    = l[0]
            position = float(l[1])
            lstart   = float(l[2])
            lend     = float(l[3])
            sigval   = float(l[4].split('=')[1])
            metref   = self.header[i-1,0]
            lrest    = float(self.header[i-1,1])
            self.table1.append([fname,position,lstart,lend,sigval,metref,lrest,i])
            self.red.append(float(l[2])/float(self.header[i-1,1])-1)
            self.red.append(float(l[3])/float(self.header[i-1,1])-1)
            i+=1
        i+=1
        ncomp=1
        while i<len(fortline) and fortline[i].split()[0]!='!' and len(fortline[i].split())!=0:
            l   = fortline[i].split('!')[0].split()
            met = l[0]+l[1] if len(l[0])==1 else l[0]
            N   = l[2]      if len(l[0])==1 else l[1]
            N   = float(re.compile(r'[^\d.-]+').sub('',N))
            z   = l[3]      if len(l[0])==1 else l[2]
            b   = l[4]      if len(l[0])==1 else l[3]
            b   = float(re.compile(r'[^\d.-]+').sub('',b))
            idx = int(l[8]) if len(l)==9    else int(l[7]) if len(l)==8 else int(l[6])
            self.table2.append([met,N,z,b,idx,ncomp])
            ncomp+=1
            i+=1
            
    def readfort26(self):
    
        fortline,fort = [],open(self.fortfile,'r')
        for line in fort:
            if len(line)==1: break
            elif line[0]!='!': fortline.append(line.replace('\n',''))
        self.table1,self.table2,self.red=[],[],[]
        i=0
        while fortline[i][0:3]=='%% ':
            l        = fortline[i].replace('%% ','').split()
            fname    = l[0]
            position = float(l[1])
            lstart   = float(l[2])
            lend     = float(l[3])
            sigval   = float(l[4].split('=')[1])
            metref   = self.header[i,0]
            lrest    = float(self.header[i,1])
            self.table1.append([fname,position,lstart,lend,sigval,metref,lrest,i+1])
            self.red.append(float(l[2])/float(self.header[i,1])-1)
            self.red.append(float(l[3])/float(self.header[i,1])-1)
            i+=1
        ncomp=1
        while i<len(fortline) and len(fortline[i])!=0:
            if '!' in fortline[i]: l = fortline[i].split('!')[0].split()
            if '[' in fortline[i]: l = fortline[i].split('[')[0].split()
            met   = l[0]+l[1]  if len(l[0])==1 else l[0]
            N     = l[6]       if len(l[0])==1 else l[5]
            N     = float(re.compile(r'[^\d.-]+').sub('',N))
            z     = l[2]       if len(l[0])==1 else l[1]
            b     = l[4]       if len(l[0])==1 else l[3]
            b     = float(re.compile(r'[^\d.-]+').sub('',b))
            idx   = int(l[10]) if len(l)==11   else int(l[9]) if len(l)==10 else int(l[8]) if len(l)==9 else int(l[7])
            self.table2.append([met,N,z,b,idx,ncomp])
            ncomp+=1
            i=i+1
            
    def createchunks(self):
    
        opfile = open('fitcommands','w')
        opfile.write('d\n\n\n'+self.fortfile+'\ny\n\nas\n\n\n')
        for i in range (1,len(self.table1)):
            opfile.write('\n\n\n\n')
            i=i+1
        opfile.write('n\nn\n')
        opfile.close()
        os.system('vpfit10 < fitcommands > termout')
        chunkfolder = 'chunks'+self.fortfile.split('.')[-1]
        os.system('mkdir -p '+chunkfolder)
        os.system('mv vpfit_chunk* '+chunkfolder+'/')
        
    def plotsetup(self,i,p):
    
        fig = self.fig
        if p==1:        # SiIV 1393.76018
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,12,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.yaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ybot = 0.4
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==2:        # SiIV 1402.77291
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,17,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
            ax.yaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ybot = 0.5
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==3:        # CIV  1548.2049
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,11,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.yaxis.set_major_locator(plt.FixedLocator([1]))
            ax2 = ax.twinx()
            ybot = 0.7
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==4:        # CIV  1550.77845
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,16,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
            ax.yaxis.set_major_locator(plt.FixedLocator([1]))
            ax2 = ax.twinx()
            ybot = 0.8
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==5:        # CII  1334.5323
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,13,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.yaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ybot = 0
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==6:        # CII  1036.3367
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,18,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
            ax.yaxis.set_major_locator(NullLocator())
            ax.set_xlabel(r'Velocity relative to $z_\mathrm{abs}='+str(round(self.zmid,6))+'$ [km/s]',fontsize=10)
            ax2 = ax.twinx()
            ybot = 0
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==7:        # SiII 1526.70698
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,19,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
            ax.yaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ybot = 0.7
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==8:        # SiII 1260.4221
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,14,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.yaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ybot = 0
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==9:        # AlII 1670.7886
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,15,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            plt.setp(ax.get_xticklabels(), visible=False)
            ax.yaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ybot = 0.7
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if p==10:        # HI   1215.6701
            ax = fig.add_subplot(5,1,1,xlim=[-350,500],ylim=[-0.15,2])
            axhline(y=0,color='black',ls='dotted')
            axhline(y=1,color='black',ls='dotted')
            ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
            ax.xaxis.set_major_locator(NullLocator())
            ax2 = ax.twinx()
            ax2.set_xlim(-350,500)
            ax2.set_ylim(-0.15,2)
            ax2.yaxis.set_major_locator(NullLocator())
        if p==11:        # HI   1025.7223
            ax = fig.add_subplot(5,1,2,xlim=[-350,500],ylim=[-0.15,2])
            axhline(y=0,color='black',ls='dotted')
            axhline(y=1,color='black',ls='dotted')
            ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
            ax.xaxis.set_major_locator(plt.FixedLocator([-400,-300,-200,-100,0,100,200,300,400,500,600]))
            ax.set_xlabel(r'Velocity relative to $z_\mathrm{abs}='+str(round(self.zmid,6))+'$ [km/s]',fontsize=10)
            ax2 = ax.twinx()
            ax2.set_xlim(-350,500)
            ax2.set_ylim(-0.15,2)
            ax2.yaxis.set_major_locator(NullLocator())
        if p==12:        # OI   988
            xmin,xmax = -100,100
            ax = fig.add_subplot(4,5,20,xlim=[xmin,xmax],ylim=[-0.15,2.5])
            ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
            ax.yaxis.set_major_locator(NullLocator())                 
            ax2 = ax.twinx()
            ybot = 0
            ymax = ybot + (1-ybot) * 2.5
            ymin = ymax - (1-ybot) * 2.65
            ax2.set_xlim(xmin,xmax)
            ax2.set_ylim(ymin,ymax)
            ax2.axhline(y=ybot,color='blue',ls='dotted')
            ax2.yaxis.set_major_locator(NullLocator())
        if self.layer==0 and p in [10,11]:
            ax.text(-8*350/10,1.1,'Lyman '+r'$\beta$',color='black',fontsize=10,horizontalalignment='center')
        if self.layer==0 and p in [1,2,3,4,5,6,7,8,9,12]:
            ax.text(0.95*xmin,2.1,str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),color='black',fontsize=10)
            ax2.text(0.95*xmin,ybot,'%.1f'%ybot,weight='bold',color='blue',ha='left',va='bottom',fontsize=8)
        ax.axhline(y=1,color='black',ls='dotted')
        self.ax  = ax
        self.ax2 = ax2

    def createcoaddspec(self,i):
    
        self.checkshift(i)
        wa,fl,er = self.readspec(i)
        # Middle wavelength for transition
        self.midwa = float(self.table1[i][6])*(1+self.zmid)
        # Store data values from spectrum within velocity range previously defined [-dv,dv]
        ibeg  = abs(wa-self.midwa*(-self.dv/self.c+1)).argmin()
        iend  = abs(wa-self.midwa*(+self.dv/self.c+1)).argmin()
        spwa0 = wa[ibeg:iend]
        spfl0 = fl[ibeg:iend]
        sper0 = er[ibeg:iend]
        corr  = self.cont + self.slope*(spwa0/((1+self.z)*1215.6701)-1)
        flux  = spfl0/corr + self.zero
        fmax  = 1 + self.zero
        data0 = spec.rebin(spwa0*(self.shift/self.c+1),flux/fmax,sper0,wa=spwa0)
        # Store model values from chunks within fitting range defined in the fort files
        chrep = 'chunks26' if self.forttype=='26' else 'chunks13'
        fit   = np.loadtxt(chrep+'/vpfit_chunk'+'%03d'%(i+1)+'.txt',comments='!')
        ibeg  = abs(fit[:,0]-self.table1[i][2]).argmin()
        iend  = abs(fit[:,0]-self.table1[i][3]).argmin()
        wave  = fit[ibeg:iend,0]
        flux  = fit[ibeg:iend,3]
        error = fit[ibeg:iend,2]
        if self.forttype=='26':
            corr  = self.cont + self.slope*(wave/((1+self.z)*1215.6701)-1)
            flux  = flux/corr + self.zero
            fmax  = 1 + self.zero
            fit0  = spec.rebin(wave*(self.shift/self.c+1),flux/fmax,error,wa=spwa0)
        if self.forttype=='13':
            fit0  = spec.rebin(wave,flux,error,wa=spwa0)
        minfitreg = []
        maxfitreg = []
        minfitreg.append(self.table1[i][2])
        maxfitreg.append(self.table1[i][3])
        i=i+1
        while (i<len(self.table1) and self.table1[i][6]==self.table1[i-1][6]):
            self.checkshift(i)
            wa,fl,er = self.readspec(i)
            # Store data values from spectrum using same velocity interval range,
            # and combine with the previous data array
            ibeg  = abs(wa-self.midwa*(-self.dv/self.c+1)).argmin()
            iend  = abs(wa-self.midwa*(+self.dv/self.c+1)).argmin()
            wave  = wa[ibeg:iend]
            flux  = fl[ibeg:iend]
            error = er[ibeg:iend]
            if self.forttype=='26':
                corr  = self.cont + self.slope*(wave/((1+self.z)*1215.6701)-1)
                flux  = flux/corr + self.zero
                fmax  = 1 + self.zero
                data1 = spec.rebin(wave*(self.shift/self.c+1),flux/fmax,error,wa=spwa0)
            if self.forttype=='13':
                data1 = spec.rebin(wave,flux,error,wa=spwa0)
            data0 = spec.combine([data0,data1])
            # Store model values from chunks using fort fitting interval
            chrep = 'chunks26' if self.forttype=='26' else 'chunks13'
            fit   = np.loadtxt(chrep+'/vpfit_chunk'+'%03d'%(i+1)+'.txt',comments='!')
            ibeg  = abs(fit[:,0]-self.table1[i][2]).argmin()            
            iend  = abs(fit[:,0]-self.table1[i][3]).argmin()
            wave  = fit[ibeg:iend,0]
            flux  = fit[ibeg:iend,3]
            error = fit[ibeg:iend,2]
            if self.forttype=='26':
                corr  = self.cont + self.slope*(wave/((1+self.z)*1215.6701)-1)
                flux  = flux/corr + self.zero
                fmax  = 1 + self.zero
                fit1  = spec.rebin(wave*(self.shift/self.c+1),flux/fmax,error,wa=spwa0)
            if self.forttype=='13':
                fit1  = spec.rebin(wave,flux,error,wa=spwa0)
            fit0  = spec.combine([fit0,fit1])
            minfitreg.append(self.table1[i][2])
            maxfitreg.append(self.table1[i][3])
            if i+1<len(self.table1) and self.table1[i+1][2]==self.table1[i+1][3]:
                i=i+1
            i=i+1
        os.system('mkdir -p ./coadd/')
        self.coadd = './coadd/trans_'+self.table1[i-1][5]+'_'+str(round(float(self.table1[i-1][6]),2))+'.dat'
        opfile = open(self.coadd, 'w')
        for j in range (0,len(fit0.wa)):
            wave  = data0.wa[j]
            flux  = data0.fl[j]
            error = data0.er[j]
            model = fit0.fl[j]
            opfile.write('{0:<15} {1:>15} {2:>15} {3:>15}\n'.format('%.8f'%wave,'%.12f'%flux,'%.12f'%error,'%.12f'%model))
        opfile.close()
        return i
    
    def checkshift(self,i):
    
        left  = self.table1[i][2]
        right = self.table1[i][3]
        self.shift,self.cont,self.slope,self.z,self.zero = 0,1,0,0,0
        for j in range(0,len(self.table2)):
            val = self.table2[j][0]
            N   = float(re.compile(r'[^\d.-]+').sub('',str(self.table2[j][1])))
            z   = float(re.compile(r'[^\d.-]+').sub('',str(self.table2[j][2])))
            b   = float(re.compile(r'[^\d.-]+').sub('',str(self.table2[j][3])))
            idx = self.table2[j][4]
            if val==">>" and ((self.table2[j][4]==0 and left<1215.6701*(z+1)<right) or self.table2[j][4]==i+1):
                self.shift = -b
            if val=="<>" and ((self.table2[j][4]==0 and left<1215.6701*(z+1)<right) or self.table2[j][4]==i+1):
                self.cont  = N
                self.slope = b
                self.z     = z
            if val=="__" and ((self.table2[j][4]==0 and left<1215.6701*(z+1)<right) or self.table2[j][4]==i+1):
                self.zero  = -N
        print self.table1[i][5],self.table1[i][6],self.table1[i][0],'vpfit_chunk'+'%03d'%(i+1)

    def readspec(self,i):
        
        if self.table1[i][0].split('.')[-1]=='txt':
            data = np.loadtxt(self.table1[i][0],comments='!')     # Read spectrum for data plotting
            wa,fl,er = data[:,0],data[:,1],data[:,2]
        if '.fits' in self.table1[i][0] and os.path.exists(self.table1[i][0].replace('.fits','.wav.fits'))==True:
            fh = fits.open(self.table1[i][0].replace('.fits','.wav.fits'))
            hd = fh[0].header
            wa = fh[0].data
            fh = fits.open(self.table1[i][0])
            hd = fh[0].header
            fl = fh[0].data
            fh = fits.open(self.table1[i][0].replace('.fits','.sig.fits'))
            hd = fh[0].header
            er = fh[0].data
        elif self.table1[i][0].split('.')[-1]=='fits':
            fh = fits.open(self.table1[i][0])
            hd = fh[0].header
            d  = fh[0].data
            wa = 10**(hd['CRVAL1'] + (hd['CRPIX1'] - 1 + np.arange(hd['NAXIS1']))*hd['CDELT1'])
            fl = d[:]
            fh = fits.open(self.table1[i][0].replace('fits','sig.fits'))
            d  = fh[0].data
            er = d[:]
        return wa,fl,er    
            
    def plot(self,i):
        
        ax  = self.ax
        ax2 = self.ax2
        val = np.loadtxt(self.coadd)
        vel = (val[:,0]-self.midwa)/self.midwa*self.c
        if self.layer==0:
            pos = abs(val[:,0]-self.midwa).argmin()
            ax2.plot(vel+(vel[pos]-vel[pos-1])/2,val[:,1],drawstyle='steps',lw=0.8,color="black")
            #plot(vel+(vel[pos]-vel[pos-1])/2,val[:,1],drawstyle='steps',lw=0.8,color="magenta")
        if self.layer==1:
            vbeg = abs(val[:,0]-self.table1[i-1][2]).argmin()+1
            vend = abs(val[:,0]-self.table1[i-1][3]).argmin()-1
            ax2.plot(vel[vbeg:vend],val[vbeg:vend,3],lw=.7,color='orange')  #initial color: #ff1493
        if self.layer==2:
            self.vertlines(i)
            #if self.table1[i-1][5]!=header[-1,0]:
            vbeg = abs(val[:,0]-self.table1[i-1][2]).argmin()+1
            vend = abs(val[:,0]-self.table1[i-1][3]).argmin()-1
            ax2.plot(vel[vbeg:vend],val[vbeg:vend,3],lw=.7,color="Lime")
            res = (val[vbeg:vend,1]-val[vbeg:vend,3])/val[vbeg:vend,2]/10+1.7
            ax.plot(vel[vbeg:vend],res,lw=0.8,c='slategrey')
            ax.axhline(y=1.6,color='red')
            ax.axhline(y=1.7,color='red',ls='dotted')
            ax.axhline(y=1.8,color='red')
    
    def vertlines(self,i):
    
        val,ax = 1,self.ax
        for j in range(0,len(self.table2)):
            if self.table2[j][0] not in ['<>','<<','>>','__','??']:
                red = float(re.compile(r'[^\d.-]+').sub('',self.table2[j][2]))
                for k in range(0,len(self.atom)):
                    cond1 = self.table2[j][0]==self.atom[k][0]
                    cond2 = self.atom[k][1]*(red+1)>self.table1[i-1][2]
                    cond3 = self.atom[k][1]*(red+1)<self.table1[i-1][3]
                    if cond1==True and cond2==True and cond3==True:
                        lobs = self.atom[k][1]*(red+1)
                        vobs = (lobs-self.midwa)/self.midwa*self.c
                        if self.table2[j][2][-1].isdigit()==True:
                            ax.axvline(x=vobs,ls='dotted',color='grey',lw=1,alpha=.8)
                        else:
                            if val%2==0: ax.text(vobs,1.08,str(self.table2[j][2][-2].upper()),family='sans-serif',color='#424242',fontsize=8,ha='center')
                            if val%2!=0: ax.text(vobs,1.2,str(self.table2[j][2][-2].upper()), family='sans-serif',color='#424242',fontsize=8,ha='center')
                            val = val + 1
                            if self.table2[j][0]=='HI':
                                line = ax.axvline(x=vobs,ls='dashed',color='darkorange',lw=.8,alpha=.8)
                                line.set_dashes([8, 4, 2, 4, 2, 4])
                            elif self.table2[j][0]=='DI':
                                line = ax.axvline(x=vobs,ls='dashed',color='blue',lw=.8,alpha=.8)
                                line.set_dashes([8, 4, 2, 4, 2, 4])
                            else:
                                line = ax.axvline(x=vobs,ls='dashed',color='darkturquoise',lw=.5)
                                line.set_dashes([8, 4, 2, 4, 2, 4])                        
    
