#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def lowz_plot(k,i):
    
    ax  = self.ax
    ax2 = self.ax2
           
    val = np.loadtxt(self.coadd)
    vel = (val[:,0]-self.midwa)/self.midwa*self.c

    if k==2:
        pos = abs(val[:,0]-self.midwa).argmin()
        ax2.plot(vel+(vel[pos]-vel[pos-1])/2,val[:,1],drawstyle='steps',lw=0.8,color="black")
#        plot(vel+(vel[pos]-vel[pos-1])/2,val[:,1],drawstyle='steps',lw=0.8,color="magenta")
                 
    if k==4:
        vbeg = abs(val[:,0]-self.table1[i-1][2]).argmin()+1
        vend = abs(val[:,0]-self.table1[i-1][3]).argmin()-1
        ax2.plot(vel[vbeg:vend],val[vbeg:vend,3],lw=.7,color='orange')  #initial color: #ff1493
        
    if k==6:
        
        self.vertlines(i)
        
#        if self.table1[i-1][5]!=header[-1,0]:

        vbeg = abs(val[:,0]-self.table1[i-1][2]).argmin()+1
        vend = abs(val[:,0]-self.table1[i-1][3]).argmin()-1
        ax2.plot(vel[vbeg:vend],val[vbeg:vend,3],lw=.7,color="Lime")
    
        res = (val[vbeg:vend,1]-val[vbeg:vend,3])/val[vbeg:vend,2]/10+1.7
        ax.plot(vel[vbeg:vend],res,lw=0.8,c='slategrey')

        ax.axhline(y=1.6,color='red')
        ax.axhline(y=1.7,color='red',ls='dotted')
        ax.axhline(y=1.8,color='red')
    
#==================================================================================================================
