#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def checkshift(i):

    left  = self.table1[i][2]
    right = self.table1[i][3]
    
    self.shift,self.cont,self.slope,self.z,self.zero = 0,1,0,0,0
    
    for j in range(0,len(self.table2)):
        
        val = self.table2[j][0]
        N   = float(re.compile(r'[^\d.-]+').sub('',str(self.table2[j][1])))
        z   = float(re.compile(r'[^\d.-]+').sub('',str(self.table2[j][2])))
        b   = float(re.compile(r'[^\d.-]+').sub('',str(self.table2[j][3])))
        idx = self.table2[j][4]
        
        if val==">>" and ((self.table2[j][4]==0 and left<1215.6701*(z+1)<right) or self.table2[j][4]==i+1):
            self.shift = -b
        if val=="<>" and ((self.table2[j][4]==0 and left<1215.6701*(z+1)<right) or self.table2[j][4]==i+1):
            self.cont  = N
            self.slope = b
            self.z     = z
        if val=="__" and ((self.table2[j][4]==0 and left<1215.6701*(z+1)<right) or self.table2[j][4]==i+1):
            self.zero  = -N
            
    print self.table1[i][5],self.table1[i][6],self.table1[i][0],'vpfit_chunk'+'%03d'%(i+1)

#==================================================================================================================
