#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def createcoaddspec(k,i):

    checkshift(i)
    wa,fl,er = self.readspec(i)
    
    ''' Middle wavelength for transition '''
    
    self.midwa = float(self.table1[i][6])*(1+self.zmid)

    ''' Store data values from spectrum within velocity range previously defined [-dv,dv] '''
    
    ibeg  = abs(wa-self.midwa*(-self.dv/self.c+1)).argmin()
    iend  = abs(wa-self.midwa*(+self.dv/self.c+1)).argmin()
    spwa0 = wa[ibeg:iend]
    spfl0 = fl[ibeg:iend]
    sper0 = er[ibeg:iend]
    corr  = self.cont + self.slope*(spwa0/((1+self.z)*1215.6701)-1)
    flux  = spfl0/corr + self.zero
    fmax  = 1 + self.zero
    data0 = spec.rebin(spwa0*(self.shift/self.c+1),flux/fmax,sper0,wa=spwa0)
    
    ''' Store model values from chunks within fitting range defined in the fort files '''
    
    fit   = np.loadtxt(sys.argv[k+1]+'/vpfit_chunk'+'%03d'%(i+1)+'.txt',comments='!')
    ibeg  = abs(fit[:,0]-self.table1[i][2]).argmin()
    iend  = abs(fit[:,0]-self.table1[i][3]).argmin()
    wave  = fit[ibeg:iend,0]
    flux  = fit[ibeg:iend,3]
    error = fit[ibeg:iend,2]
    if self.forttype=='26':
        corr  = self.cont + self.slope*(wave/((1+self.z)*1215.6701)-1)
        flux  = flux/corr + self.zero
        fmax  = 1 + self.zero
        fit0  = spec.rebin(wave*(self.shift/self.c+1),flux/fmax,error,wa=spwa0)
    if self.forttype=='13':
        fit0  = spec.rebin(wave,flux,error,wa=spwa0)

    minfitreg = []
    maxfitreg = []
    minfitreg.append(self.table1[i][2])
    maxfitreg.append(self.table1[i][3])

    i=i+1
    while (i<len(self.table1) and self.table1[i][6]==self.table1[i-1][6]):
        
        checkshift(i)
        wa,fl,er = self.readspec(i)

        ''' Store data values from spectrum using same velocity interval range, and combine with the previous data array '''
        
        ibeg  = abs(wa-self.midwa*(-self.dv/self.c+1)).argmin()
        iend  = abs(wa-self.midwa*(+self.dv/self.c+1)).argmin()
        wave  = wa[ibeg:iend]
        flux  = fl[ibeg:iend]
        error = er[ibeg:iend]
        if self.forttype=='26':
            corr  = self.cont + self.slope*(wave/((1+self.z)*1215.6701)-1)
            flux  = flux/corr + self.zero
            fmax  = 1 + self.zero
            data1 = spec.rebin(wave*(self.shift/self.c+1),flux/fmax,error,wa=spwa0)
        if self.forttype=='13':
            data1 = spec.rebin(wave,flux,error,wa=spwa0)

        data0 = spec.combine([data0,data1])

        ''' Store model values from chunks using fort fitting interval '''
        
        fit   = np.loadtxt(sys.argv[k+1]+'/vpfit_chunk'+'%03d'%(i+1)+'.txt',comments='!')
        ibeg  = abs(fit[:,0]-self.table1[i][2]).argmin()            
        iend  = abs(fit[:,0]-self.table1[i][3]).argmin()
        wave  = fit[ibeg:iend,0]
        flux  = fit[ibeg:iend,3]
        error = fit[ibeg:iend,2]
        if self.forttype=='26':
            corr  = self.cont + self.slope*(wave/((1+self.z)*1215.6701)-1)
            flux  = flux/corr + self.zero
            fmax  = 1 + self.zero
            fit1  = spec.rebin(wave*(self.shift/self.c+1),flux/fmax,error,wa=spwa0)
        if self.forttype=='13':
            fit1  = spec.rebin(wave,flux,error,wa=spwa0)
        fit0  = spec.combine([fit0,fit1])

        minfitreg.append(self.table1[i][2])
        maxfitreg.append(self.table1[i][3])
        
        if i+1<len(self.table1) and self.table1[i+1][2]==self.table1[i+1][3]:
            i=i+1

        i=i+1

    if os.path.exists('./coadd/')==False:
        os.system('mkdir ./coadd/')

    self.coadd = './coadd/trans_'+self.table1[i-1][5]+'_'+str(round(float(self.table1[i-1][6]),2))+'.dat'

    opfile = open(self.coadd, 'w')
    for j in range (0,len(fit0.wa)):
        wave  = data0.wa[j]
        flux  = data0.fl[j]
        error = data0.er[j]
        model = fit0.fl[j]
        opfile.write('{0:<15} {1:>15} {2:>15} {3:>15}\n'.format('%.8f'%wave,'%.12f'%flux,'%.12f'%error,'%.12f'%model))
    opfile.close()

    return i

#==================================================================================================================
