#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def readfort13():

    fort = open(self.fortfile,'r')
    fortline = []
    for line in fort:
        if len(line)==1: break
        elif line[0]!='!': fortline.append(line.replace('\n',''))
    
    self.table1,self.table2=[],[]
    wav,self.red=[],[]

    i=1
    while fortline[i].split()[0]!='*':
        l = fortline[i].split()
        self.table1.append([l[0],                       # filename
                       float(l[1]),                     # position
                       float(l[2]),                     # lambinit
                       float(l[3]),                     # lambfina
                       float(l[4].split('=')[1]),       # sigvalue
                       self.header[i-1,0],              # metalref
                       float(self.header[i-1,1]),       # wavelength
                       i])                              # region ID
        wav.append(float(l[2]))
        wav.append(float(l[3]))
        self.red.append(float(l[2])/float(self.header[i-1,1])-1)
        self.red.append(float(l[3])/float(self.header[i-1,1])-1)
        i=i+1
    
    i=i+1
    while i<len(fortline):
        l = fortline[i].split()
        s = fortline[i].split('!')
        
        # If the species is 1 letter (H,C,S) and fort.13 DOES have varying alpha column
        
        if len(l[0])==1 and len(s[0].split())==9:  
            self.table2.append([l[0]+l[1],                                     # metalref
                                float(re.compile(r'[^\d.-]+').sub('',l[2])),   # coldensit
                                l[3],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[4])),   # dopparam,
                                float(l[8]),                                   # transind,
                                l[10]])                                        # numbcomp
                                
        # If the species is 1 letter (H,C,S) and fort.13 has NO varying alpha column
        
        if len(l[0])==1 and len(s[0].split())==8:      
            self.table2.append([l[0]+l[1],                                     # metalref
                                float(re.compile(r'[^\d.-]+').sub('',l[2])),   # coldensit
                                l[3],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[4])),   # dopparam,
                                float(l[7]),                                   # transind,
                                l[9]])                                         # numbcomp

        # If the species is 2 letters (Al,Mg,Fe) and fort.13 DOES have varying alpha column
                                 
        if len(l[0])>1  and len(s[0].split())==8:
            self.table2.append([l[0],                                          # metalref
                                float(re.compile(r'[^\d.-]+').sub('',l[1])),   # coldensit
                                l[2],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[3])),   # dopparam,
                                float(fortline[i].split()[7]),                 # transind,
                                fortline[i].split()[9]])                       # numbcomp

        # If the species is 2 letters (Al,Mg,Fe) and fort.13 has NO varying alpha column
        
        if len(l[0])>1  and len(s[0].split())==7:        
            self.table2.append([l[0],                                          # metalref
                                float(re.compile(r'[^\d.-]+').sub('',l[1])),   # coldensit
                                l[2],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[3])),   # dopparam,
                                float(l[6]),                                   # transind,
                                l[8]])                                         # numbcomp
        i=i+1

#==================================================================================================================
