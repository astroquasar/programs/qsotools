#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def LL_plotsetup(k,i,p):
        
    fig = self.fig
    
    if p==1:
        print 'HERE'
        ax = fig.add_subplot(1,1,1,xlim=[4173,4200],ylim=[-0.15,1.15])
#        ax.xaxis.set_major_locator(plt.FixedLocator([-100,0,100]))
#        ax.yaxis.set_major_locator(NullLocator())
#        text(-0.9*500,2.,'Lyman Limit',color='black',fontsize=10)
        ax.set_xlabel(r'Wavelength [$\AA$]',fontsize=10)

    axhline(y=1,color='black',ls='dotted')
    axhline(y=0,color='black',ls='dotted')
    
    self.ax  = ax
    
#==================================================================================================================
