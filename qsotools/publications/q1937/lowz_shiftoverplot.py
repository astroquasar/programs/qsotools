#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def lowz_shiftoverplot():
        
    '''Initialise spec and define all constants to be used later'''

                
    self.setup2  = np.loadtxt('data/setup2.txt',  usecols=(0,1,3))
    self.setup3  = np.loadtxt('data/setup3.txt',  usecols=(0,1,3))
    self.setup5  = np.loadtxt('data/setup5.txt',  usecols=(0,1,3))
    self.setup10 = np.loadtxt('data/setup10.txt',usecols=(0,1,3))
    
    fh = fits.open('../../data/J193957-100241.fits')
    d = fh[0].data
    d.shape    
    hd = fh[0].header
    wa = 10**(hd['CRVAL1'] + (hd['CRPIX1'] - 1 + np.arange(hd['NAXIS1']))*hd['CDELT1'])
    co = d[3,:]
    fl = d[0,:]
    er = d[1,:]
    
    self.vlt = np.array(np.transpose([wa,fl,co]))
    
    self.shift = [0,-1.5,0.3,-0.6,0.15]
    
    ''' velocity shifts for setup2, setup3, setup5, setup10, vlt '''
    
    center = 5176.9
    width = 500
    norms = 1./np.array([1.01, 1.01,1.01,1.01,0.96])
    name='Lyalpha_all'
    figplot(name,center,width,norms)
    
    center = 4365
    width = 500
    norms = 1./np.array([0.99, 1.02, 1.01, 1.01, 0.97])
    name ='Lybeta_all'
    figplot(name,center,width,norms)
    
    center = 4410.6
    width = 300
    norms = 1./np.array([0.90, 0.92, 1.0,0.96, 0.92])
    name='CII_1036_all'
    figplot(name,center,width,norms)
    
    center = 5679.6
    width = 180
    norms = 1./np.array([0.98, 0.98, 0.98,0.97, 0.96])
    name='CII_1334_all'
    figplot(name,center,width,norms)
    
    center = 5364.4
    width = 180
    norms = 1./np.array([1,1,1,1,1])
    name='SiII_1260'
    figplot(name,center,width,norms)
    
    center = 6497.6
    width = 180
    norms = 1./np.array([1,1,1,1,1])
    name='SiII_1526'
    figplot(name,center,width,norms)
    
    center = 5931.7
    width = 180
    norms = 1./np.array([1,1,1,1,1])
    name='SiIV_1393'
    figplot(name,center,width,norms)
    
    center = 5970.2
    width = 180
    norms = 1./np.array([1,1,1,1,1])
    name='SiIV_1402'
    figplot(name,center,width,norms)
    
    center = 6589.4
    width = 180
    norms = 1./np.array([1,1,1,1,1])
    name='CIV_1548'
    figplot(name,center,width,norms)
    
    center = 6600.3
    width = 180
    norms = 1./np.array([1,1,1,1,1])
    name='CIV_1550'
    figplot(name,center,width,norms)

#==================================================================================================================
