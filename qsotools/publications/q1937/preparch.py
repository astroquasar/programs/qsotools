#!/usr/bin/env python

#==================================================================================================================

import sys,os,re
import numpy                           as np
import matplotlib.pyplot               as plt
import matplotlib.gridspec             as gridspec
from matplotlib.pyplot                 import *
from barak                             import spec
from pylab                             import *
from datetime                          import datetime
from matplotlib                        import rc
from astropy.io                        import fits
from matplotlib.backends.backend_pdf   import PdfPages

#==================================================================================================================

from checkshift                  import *
from createchunks                import *
from createcoaddspec             import *
from highz_plot                  import *
from highz_plotsetup             import *
from limit_plot                  import *
from limit_plotsetup             import *
from lowz_figplot                import *
from lowz_plot                   import *
from lowz_plotsetup              import *
from lowz_shiftsetup             import *
from lowz_shiftoverplot          import *
from readfort13                  import *
from readfort26                  import *
from readspec                    import *
from vertlines                   import *

#==================================================================================================================

rc('font', size=2, family='sans-serif')
rc('axes', labelsize=2, linewidth=0.5)
rc('legend', fontsize=2, handlelength=10)
rc('xtick', labelsize=8)
rc('ytick', labelsize=8)
rc('lines', lw=0.5, mew=0.2)
rc('grid', linewidth=0.5)

#==================================================================================================================

os.environ['VPFSETUP']='./vp_setup.dat'
os.environ['ATOMDIR']='./atom.dat'

k = 1.3806488*10**-23     # m^2.kg.s^-2.K^-1
c = 299792.458            # km/s

#==================================================================================================================

lowz_header = np.array([['SiIV', 1393.76018 ],
                        ['SiIV', 1393.76018 ],
                        ['SiIV', 1393.76018 ],
                        ['SiIV', 1393.76018 ],
                        ['SiIV', 1402.77291 ],
                        ['SiIV', 1402.77291 ],
                        ['SiIV', 1402.77291 ],
                        ['SiIV', 1402.77291 ],
                        ['CIV',  1548.2049  ],
                        ['CIV',  1548.2049  ],
                        ['CIV',  1550.77845 ],
                        ['CIV',  1550.77845 ],
                        ['CII',  1334.5323  ],
                        ['CII',  1334.5323  ],
                        ['CII',  1334.5323  ],
                        ['CII',  1334.5323  ],
                        ['CII',  1036.3367  ],
                        ['CII',  1036.3367  ],
                        ['CII',  1036.3367  ],
                        ['SiII', 1526.70698 ],
                        ['SiII', 1526.70698 ],
                        ['SiII', 1526.70698 ],
                        ['SiII', 1260.4221  ],
                        ['SiII', 1260.4221  ],
                        ['SiII', 1260.4221  ],
                        ['SiII', 1260.4221  ],
                        ['AlII', 1670.7886  ],
                        ['HI',   1215.6701  ],
                        ['HI',   1215.6701  ],
                        ['HI',   1215.6701  ],
                        ['HI',   1215.6701  ],
                        ['HI',   1025.7223  ],
                        ['HI',   1025.7223  ],
                        ['HI',   1025.7223  ],
                        ['HI',   1025.7223  ],
                        ['OI',    988.7734  ]])

lowz_atom = [['HI',  1215.6701 ],
             ['HI',  1025.7223 ],
             ['DI',  1215.3394 ],
             ['DI',  1025.4433 ],
             ['CII', 1334.5323 ],
             ['CII', 1036.3367 ],
             ['CIV', 1548.2049 ],
             ['CIV', 1550.77845],
             ['SiII',1260.4221 ],
             ['SiII',1526.70698],
             ['SiIV',1393.76018],
             ['SiIV',1402.77291],
             ['AlII',1670.7886 ],
             ['OI',   988.7734 ]]

#==================================================================================================================

rc('font', size=2, family='sans-serif')
rc('axes', labelsize=2, linewidth=0.5)
rc('legend', fontsize=2, handlelength=10)
rc('xtick', labelsize=8)
rc('ytick', labelsize=8)
rc('lines', lw=0.5, mew=0.2)
rc('grid', linewidth=0.5)

#==================================================================================================================

os.environ['VPFSETUP']='./vp_setup.dat'
os.environ['ATOMDIR']='./atom.dat'

k = 1.3806488*10**-23     # m^2.kg.s^-2.K^-1
c = 299792.458            # km/s

#==================================================================================================================
highz_header = np.array([['CII',      1334.5323   ],
                         ['CII',      1334.5323   ],
                         ['CII',      1334.5323   ],
                         ['CII',      1334.5323   ],
                         ['CII',      1334.5323   ],
                         ['FeIII',    1122.524    ],
                         ['FeIII',    1122.524    ],
                         ['FeIII',    1122.524    ],
                         ['FeIII',    1122.524    ],
                         ['FeIII',    1122.524    ],
                         ['SiIV',     1402.77291  ],
                         ['SiIV',     1402.77291  ],
                         ['SiIV',     1402.77291  ],
                         ['SiIV',     1393.76018  ],
                         ['SiIV',     1393.76018  ],
                         ['SiIV',     1393.76018  ],
                         ['SiIV',     1393.76018  ],
                         ['SiIV',     1393.76018  ],
                         ['SiII',     1304.3702   ],
                         ['SiII',     1304.3702   ],
                         ['SiII',     1304.3702   ],
                         ['SiII',     1304.3702   ],
                         ['SiII',     1304.3702   ],
                         ['SiII',     1193.2897   ],
                         ['SiII',     1193.2897   ],
                         ['SiII',     1193.2897   ],
                         ['SiII',     1193.2897   ],
                         ['SiII',     1193.2897   ],
                         ['HI',       1215.6701   ],
                         ['HI',       1215.6701   ],
                         ['HI',       1215.6701   ],
                         ['HI',       1215.6701   ],
                         ['HI',       1215.6701   ],
                         ['HI',       1025.7223   ],
                         ['HI',       1025.7223   ],
                         ['HI',       1025.7223   ],
                         ['HI',       1025.7223   ],
                         ['HI',        972.5368   ],
                         ['HI',        972.5368   ],
                         ['HI',        972.5368   ],
                         ['HI',        972.5368   ],
                         ['HI',        972.5368   ],
                         ['HI',        949.7431   ],
                         ['HI',        949.7431   ],
                         ['HI',        949.7431   ],
                         ['HI',        949.7431   ],
                         ['HI',        949.7431   ],
                         ['HI',        937.8035   ],
                         ['HI',        937.8035   ],
                         ['HI',        937.8035   ],
                         ['HI',        937.8035   ],
                         ['HI',        937.8035   ],
                         ['HI',        930.7483   ],
                         ['HI',        930.7483   ],
                         ['HI',        930.7483   ],
                         ['HI',        930.7483   ],
                         ['HI',        930.7483   ],
                         ['HI',        926.2257   ],
                         ['HI',        926.2257   ],
                         ['HI',        926.2257   ],
                         ['HI',        926.2257   ],
                         ['HI',        926.2257   ],
                         ['HI',        923.1504   ],
                         ['HI',        923.1504   ],
                         ['HI',        923.1504   ],
                         ['HI',        923.1504   ],
                         ['HI',        923.1504   ],
                         ['HI',        920.9631   ],
                         ['HI',        920.9631   ],
                         ['HI',        920.9631   ],
                         ['HI',        920.9631   ],
                         ['HI',        920.9631   ]],dtype=object)

highz_atom = [['CII',      1334.5323   ],
              ['FeIII',    1122.524    ],
              ['SiIV',     1402.77291  ],
              ['SiIV',     1393.76018  ],
              ['SiII',     1304.3702   ],
              ['SiII',     1193.2897   ],
              ['HI',       1215.6701   ],
              ['HI',       1025.7223   ],
              ['HI',        972.5368   ],
              ['HI',        949.7431   ],
              ['HI',        937.8035   ],
              ['HI',        930.7483   ],
              ['HI',        926.2257   ],
              ['HI',        923.1504   ],
              ['HI',        920.9631   ],
              ['DI',       1215.3394   ],
              ['DI',       1025.4433   ],
              ['DI',        972.2722   ],
              ['DI',        949.4847   ],
              ['DI',        937.5484   ],
              ['DI',        930.4951   ],
              ['DI',        925.9737   ],
              ['DI',        922.899    ],
              ['DI',        920.712    ]]
    
#==================================================================================================================

LL_header = np.array([   ['HI',        915.329   ],
                         ['HI',        915.329   ],
                         ['HI',        915.329   ],
                         ['HI',        915.329   ],
                         ['HI',        915.329   ],
                         ['HI',        915.329   ]],dtype=object)

LL_atom =                [['HI',        915.329   ]]
