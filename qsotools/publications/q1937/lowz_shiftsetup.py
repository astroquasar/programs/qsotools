#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def lowz_shiftsetup():

    name  = np.loadtxt('transition_name.txt',dtype='str')
    shift = np.loadtxt('transition_vel_shifts.txt',dtype='float')
    
    for i in range (0,len(name)):
    
        print name[i],shift[i]
        spec    = np.loadtxt('data/'+name[i])
        newname = name[i].split('.')[0]+'_shift.'+name[i].split('.')[1]
        ipfile = open('shifted/'+newname, 'w')
        newwav = spec[:,0]*(shift[i]/299792.458+1)
        for j in range(0,len(spec)):
            ipfile.write(str(newwav[j])+'  '+str(spec[j,1])+'  '+str(spec[j,2])+'\n')
        ipfile.close()

#==================================================================================================================
