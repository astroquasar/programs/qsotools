#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def vertlines(i,p):

    ax  = self.axlist[p-1]
    val = 1
    
    for j in range(0,len(self.table2)):
            
        if self.table2[j][0] not in ['<>','<<','>>','__','??']:
                
            red = float(re.compile(r'[^\d.-]+').sub('',self.table2[j][2]))
            
            for k in range(0,len(self.atom)):
                    
                cond1 = self.table2[j][0]==self.atom[k][0]
                cond2 = self.atom[k][1]*(red+1)>self.table1[i-1][2]
                cond3 = self.atom[k][1]*(red+1)<self.table1[i-1][3]
                
                if cond1==True and cond2==True and cond3==True:
                    
                    lobs = self.atom[k][1]*(red+1)
                    vobs = (lobs-self.midwa)/self.midwa*self.c

                    if self.table2[j][2][-1].isdigit()==True:
                        ax.axvline(x=vobs,ls='dotted',color='grey',lw=1,alpha=.8)
                    else:
                        if val%2==0:
                            ax.text(vobs,1.08,str(self.table2[j][2][-1].upper()),\
                                    family='sans-serif',color='#424242',fontsize=8,ha='center')
                        if val%2!=0:
                            ax.text(vobs,1.2,str(self.table2[j][2][-1].upper()),\
                                    family='sans-serif',color='#424242',fontsize=8,ha='center')
                        val = val + 1
                        if self.table2[j][0]=='HI':
                            line = ax.axvline(x=vobs,ls='dashed',color='darkorange',lw=.8,alpha=.8)
                            line.set_dashes([8, 4, 2, 4, 2, 4])
                        elif self.table2[j][0]=='DI':
                            line = ax.axvline(x=vobs,ls='dashed',color='blue',lw=.8,alpha=.8)
                            line.set_dashes([8, 4, 2, 4, 2, 4])
                        else:
                            line = ax.axvline(x=vobs,ls='dashed',color='darkturquoise',lw=.5)
                            line.set_dashes([8, 4, 2, 4, 2, 4])                        

#==================================================================================================================
