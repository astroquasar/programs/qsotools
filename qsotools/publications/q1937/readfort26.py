#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def readfort26():

    fort = open(self.fortfile,'r')
    fortline = []
    for line in fort:
        if len(line)==1: break
        elif line[0]!='!': fortline.append(line.replace('\n',''))
    
    self.table1,self.table2=[],[]
    wav,self.red=[],[]
    
    i=0
    while fortline[i][0:3]=='%% ':
        l=fortline[i].replace('%% ','').split()
        self.table1.append([l[0],                   # filename,
                       float(l[1]),                 # position,
                       float(l[2]),                 # lambinit,
                       float(l[3]),                 # lambfina,
                       float(l[4].split('=')[1]),   # sigvalue,
                       self.header[i,0],            # metalref,
                       float(self.header[i,1]),     # waveleng
                       i+1])                        # region ID
        wav.append(float(l[2]))
        wav.append(float(l[3]))
        self.red.append(float(l[2])/float(self.header[i,1])-1)
        self.red.append(float(l[3])/float(self.header[i,1])-1)
        i=i+1

    idx = 1
    while i<len(fortline) and len(fortline[i])!=0:
        l = fortline[i].split()
        if '!' in l:
            s = fortline[i].split('!')
        if '[' in l:
            s = fortline[i].split('[')
            
        # If the species is 1 letter (H,C,S) and fort.26 DOES have varying alpha column
            
        if len(l[0])==1 and len(s[0].split())==11:  
            self.table2.append([l[0]+l[1],                                     # metalref,
                                float(re.compile(r'[^\d.-]+').sub('',l[6])),   # coldensi,
                                l[2],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[4])),   # dopparam,
                                int(l[10]),                                    # transind,
                                idx])                                          # numbcomp

        # If the species is 1 letter (H,C,S) and fort.26 has NO varying alpha column
                           
        if len(l[0])==1 and len(s[0].split())==9:   
            self.table2.append([l[0]+l[1],                                     # metalref,
                                float(re.compile(r'[^\d.-]+').sub('',l[6])),   # coldensi,
                                l[2],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[4])),   # dopparam,
                                int(l[8]),                                     # transind,
                                idx])                                          # numbcomp

        # If the species is 2 letters (Al,Mg,Fe) and fort.26 DOES have varying alpha column
                           
        if len(l[0])>1 and len(s[0].split())==10:  
            self.table2.append([l[0],                                          # metalref,
                                float(re.compile(r'[^\d.-]+').sub('',l[5])),   # coldensi,
                                l[1],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[3])),   # dopparam,
                                int(l[9]),                                     # transind,
                                idx])                                          # numbcomp

        # If the species is 2 letters (Al,Mg,Fe) and fort.26 has NO varying alpha column
                           
        if len(l[0])>1 and len(s[0].split())==8: 
            self.table2.append([l[0],                                          # metalref,
                                float(re.compile(r'[^\d.-]+').sub('',l[5])),   # coldensi,
                                l[1],                                          # redshift,
                                float(re.compile(r'[^\d.-]+').sub('',l[3])),   # dopparam,
                                int(l[7]),                                     # transind,
                                idx])                                          # numbcomp
        idx=idx+1
        i=i+1

#==================================================================================================================
