#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def LL_plot(k,i):        

    ax  = self.ax
    val = np.loadtxt(self.coadd)
    if k==2:
        
#        self.vertlines(i)
#        pos = abs(val[:,0]-self.midwa).argmin()
        ax.plot(val[:,0],val[:,1],drawstyle='steps',lw=0.8,color="black")

        beg = abs(val[:,0]-self.table1[i-1][2]).argmin()+1
        end = abs(val[:,0]-self.table1[i-1][3]).argmin()-1
        ax.plot(val[beg:end,0],val[beg:end,3],lw=.7,color="Lime")


#        if sys.argv[1]=='LRIS':
#            beg = abs(self.LRIS[:,0]-self.table1[i-1][2]).argmin()
#            end = abs(self.LRIS[:,0]-self.table1[i-1][3]).argmin()
#            ax.plot(self.LRIS[beg:end,0],self.LRIS[beg:end,1]/self.LRIS[beg:end,3],drawstyle='steps',lw=0.8,color="red")
    
#==================================================================================================================
