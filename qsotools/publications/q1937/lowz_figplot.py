#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def figplot(name,center,width,norms=[1,1,1,1,1]):

    fig = figure()
    subplots_adjust(left=0.05, right=0.97, bottom=0.07, top=0.97, hspace=0, wspace=0.05)
    rc('axes', labelsize=6, linewidth=0.2)
    rc('legend', fontsize=6, handlelength=5)
    rc('xtick', labelsize=6)
    rc('ytick', labelsize=6)
    rc('lines', lw=0.2, mew=0.2)
    rc('grid', linewidth=0.2)
    ax = fig.add_subplot(1,1,1,ylim=[-0.1,1.2])
    ax.set_xlabel('Wavelength [A]')
    ax.set_ylabel('Normalised flux')

    #to force wavelengths to be written as xxxx instead of x+xe+y
    gca().xaxis.set_major_formatter(plt.FormatStrFormatter('%d'))

    start = abs(self.setup2[:,0] - (center * (1 - width/self.c)) ).argmin()
    end   = abs(self.setup2[:,0] - (center * (1 + width/self.c)) ).argmin()
    x = self.setup2[start:end,0]
    v = (self.setup2[start:end,0] - center) / center * self.c - self.shift[0]
    y = self.setup2[start:end,1]/self.setup2[start:end,2]*norms[0]
    plot(v,y,'black',lw=0.2,drawstyle='steps-mid',label='setup2')

    start = abs(self.setup3[:,0] - (center * (1 - width/self.c)) ).argmin()
    end   = abs(self.setup3[:,0] - (center * (1 + width/self.c)) ).argmin()
    x = self.setup3[start:end,0]
    v = (self.setup3[start:end,0] - center)/ center * self.c - self.shift[1]
    y = self.setup3[start:end,1]/self.setup3[start:end,2]*norms[1]
    plot(v,y,'blue',lw=0.2,drawstyle='steps-mid',label='setup3')

    start = abs(self.setup5[:,0] - (center * (1 - width/self.c)) ).argmin()
    end   = abs(self.setup5[:,0] - (center * (1 + width/self.c)) ).argmin()
    x = self.setup5[start:end,0]
    v = (self.setup5[start:end,0] - center)/ center * self.c - self.shift[2]
    y = self.setup5[start:end,1]/self.setup5[start:end,2]*norms[2]
    plot(v,y,'red',lw=0.2,drawstyle='steps-mid',label='setup5')

    start = abs(self.setup10[:,0] - (center * (1 - width/self.c)) ).argmin()
    end   = abs(self.setup10[:,0] - (center * (1 + width/self.c)) ).argmin()
    x = self.setup10[start:end,0]
    v = (self.setup10[start:end,0] - center)/ center * self.c - self.shift[3]
    y = self.setup10[start:end,1]/self.setup10[start:end,2]*norms[3]
    plot(v,y,'green',lw=0.2,drawstyle='steps-mid',label='setup10')

    start = abs(self.vlt[:,0] - (center * (1 - width/self.c)) ).argmin()
    end   = abs(self.vlt[:,0] - (center * (1 + width/self.c)) ).argmin()
    x = self.vlt[start:end,0]
    v = (self.vlt[start:end,0] - center)/ center * self.c - self.shift[4]
    y = self.vlt[start:end,1]*norms[4]
    plot(v,y,'orange',lw=0.2,drawstyle='steps-mid',label='VLT')

    leg = ax.legend(loc='upper right',frameon=False,labelspacing=0.2)
    ax.text(start,0.9,name,fontsize=7,color='black')

    axhline(y=0,lw=0.2,color='grey')

    outfile = name+'.pdf'
    savefig(outfile)

#==================================================================================================================
