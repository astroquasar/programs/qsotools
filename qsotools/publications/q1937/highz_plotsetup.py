#!/usr/bin/env python

import preparch as self
from preparch import *

#==================================================================================================================

def highz_plotsetup(k,i,p):
        
    fig = self.fig
    
    if p==1:                 # CII      1334.5323
        
        ax = fig.add_subplot(6,3,3,xlim=[-100,100],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        ax.yaxis.tick_right()
        text(-0.9*100,2.,\
             str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),\
             color='black',fontsize=10,horizontalalignment='left')
        
    if p==2:                 # FeIII    1122.524
        
        ax = fig.add_subplot(6,3,12,xlim=[-100,100],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        ax.yaxis.tick_right()
        text(-0.9*100,2.,\
             str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),\
             color='black',fontsize=10,horizontalalignment='left')
        
    if p==3:                 # SiIV     1402.77291
        
        ax = fig.add_subplot(6,3,18,xlim=[-100,100],ylim=[-0.15,2.5])
        ax.xaxis.set_major_locator(plt.FixedLocator([-50,0,50]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        ax.yaxis.tick_right()
        text(-0.9*100,2.,\
             str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),\
             color='black',fontsize=10,horizontalalignment='left')
        
    if p==4:                 # SiIV     1393.76018
        
        ax = fig.add_subplot(6,3,15,xlim=[-100,100],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        ax.yaxis.tick_right()
        text(-0.9*100,2.,\
             str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),\
             color='black',fontsize=10,horizontalalignment='left')
        
    if p==5:                 # SiII     1304.3702
        
        ax = fig.add_subplot(6,3,9,xlim=[-100,100],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        ax.yaxis.tick_right()
        text(-0.9*100,2.,\
             str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),\
             color='black',fontsize=10,horizontalalignment='left')
        
    if p==6:                 # SiII     1193.2897
        
        ax = fig.add_subplot(6,3,6,xlim=[-100,100],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        ax.yaxis.tick_right()
        text(-0.9*100,2.,\
             str(self.table1[i][5])+' '+str(int(float(self.table1[i][6]))),\
             color='black',fontsize=10,horizontalalignment='left')
        
    if p==7:
        ax = subplot2grid((6,3),(0,0),colspan=2,xlim=[-500,500],ylim=[-0.15,2.5])
        ax.xaxis.set_major_locator(plt.FixedLocator([-400,-300,-200,-100,0,100,200,300,400]))
        ax.xaxis.tick_top()
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        text(-.9*500,2.,'Lyman '+r'$\alpha$',color='black',fontsize=10)
        
    if p==8:
        
        ax = subplot2grid((6,3),(1,0),colspan=2,xlim=[-500,500],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        text(-.9*500,2.,'Lyman '+r'$\beta$',color='black',fontsize=10)
        
    if p==9:
        
        ax = subplot2grid((6,3),(2,0),colspan=2,xlim=[-500,500],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        text(-.9*500,2.,'Lyman '+r'$\gamma$',color='black',fontsize=10)
        
    if p==10:
        
        ax = fig.add_subplot(6,3,10,xlim=[-200,200],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        text(-0.9*200,2.,'Ly-4',color='black',fontsize=10)
        
    if p==11:
        
        ax = fig.add_subplot(6,3,13,xlim=[-200,200],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        text(-0.9*200,2.,'Ly-5',color='black',fontsize=10)
        
    if p==12:
        
        ax = fig.add_subplot(6,3,16,xlim=[-200,200],ylim=[-0.15,2.5])
        ax.xaxis.set_major_locator(plt.FixedLocator([-100,0,100]))
        ax.yaxis.set_major_locator(plt.FixedLocator([0,1]))
        text(-0.9*200,2.,'Ly-6',color='black',fontsize=10)
        
    if p==13:
        
        ax = fig.add_subplot(6,3,11,xlim=[-200,200],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(NullLocator())
        text(-0.9*200,2.,'Ly-7',color='black',fontsize=10)
        
    if p==14:
        
        ax = fig.add_subplot(6,3,14,xlim=[-200,200],ylim=[-0.15,2.5])
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.yaxis.set_major_locator(NullLocator())
        text(-0.9*200,2.,'Ly-8',color='black',fontsize=10)
        
    if p==15:
        
        ax = fig.add_subplot(6,3,17,xlim=[-200,200],ylim=[-0.15,2.5])
        ax.xaxis.set_major_locator(plt.FixedLocator([-100,0,100]))
        ax.yaxis.set_major_locator(NullLocator())
        text(-0.9*200,2.,'Ly-9',color='black',fontsize=10)
        ax.set_xlabel(r'Velocity Relative to $z_\mathrm{abs}='+str(round(self.zmid,6))+'$ [km/s]',fontsize=10)

    axhline(y=1,color='black',ls='dotted')
    axhline(y=0,color='black',ls='dotted')
    (self.axlist).append(ax)

#==================================================================================================================
